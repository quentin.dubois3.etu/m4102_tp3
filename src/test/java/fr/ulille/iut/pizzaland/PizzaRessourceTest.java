package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaRessourceTest extends JerseyTest {

	private PizzaDao dao;
	private IngredientDao daoIngredient;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createTable();
		daoIngredient = BDDFactory.buildDao(IngredientDao.class);
		daoIngredient.createTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTable();
		daoIngredient.dropTable();
	}


	@Test
	public void testGetEmptyList() {
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe Response permet de traiter la réponse HTTP reçue.
		Response response = target("/pizzas").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
		});

		assertEquals(0, pizzas.size());
	}

	@Test
	public void testGetExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		pizza.setPrice(10);
		pizza.setIngredients(new ArrayList<>());
		dao.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}

	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testCreatePizza() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Reine");
		pizzaCreateDto.setPrice(0);
		List<Ingredient> ingredients = Arrays.asList(new Ingredient[] {new Ingredient("tomate")});
		daoIngredient.insertMany(ingredients);
		pizzaCreateDto.setIngredients(ingredients);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
	}

	@Test
	public void testCreateSamePizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		pizza.setPrice(0);
		//List<Ingredient> ingredients = Arrays.asList(new Ingredient[] {new Ingredient("tomate")});
		pizza.setIngredients(new ArrayList<Ingredient>());
		dao.insert(pizza);

		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteExistingPizza() {
		Pizza pizza= new Pizza();
		pizza.setName("Chorizo");
		pizza.setIngredients(new ArrayList<>());
		dao.insert(pizza);

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = dao.findById(pizza.getId());
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetPizzaName() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		pizza.setPrice(0);
		pizza.setIngredients(new ArrayList<>());
		dao.insert(pizza);

		Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Chorizo", response.readEntity(String.class));
	}
	
	@Test
	public void testGetAllPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		pizza.setPrice(0);
		pizza.setIngredients(new ArrayList<>());
		
		Pizza pizza2 = new Pizza();
		pizza2.setName("Alligatress");
		pizza2.setPrice(0);
		pizza2.setIngredients(new ArrayList<>());

		dao.insert(pizza);
		dao.insert(pizza2);

		Response response = target("pizzas").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		List<Pizza> res = response.readEntity(new GenericType<List<Pizza>>() {});
		assertEquals(res.get(0), pizza);
		assertEquals(res.get(1), pizza2);
	}

	@Test
	public void testGetNotExistingPizzaName() {
		Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
    public void testCreateWithForm() {
        Form form = new Form();
        form.param("name", "chorizo");
        form.param("price", "0.0");

        Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
        Response response = target("pizzas").request().post(formEntity);

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        String location = response.getHeaderString("Location");
        String id = location.substring(location.lastIndexOf('/') + 1);
        Pizza result = dao.findById(UUID.fromString(id));

        assertNotNull(result);
    }


}
