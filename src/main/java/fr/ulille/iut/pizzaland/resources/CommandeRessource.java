package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Path("/commandes")
public class CommandeRessource {

	private CommandeDao commandes;
	
	@Context
	public UriInfo uriInfo;

	public CommandeRessource() {
		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createTable();
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public CommandeDto getOneCommande(@PathParam("id") UUID id) {
		try {
			return Commande.toDto(commandes.findById(id));
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	public List<CommandeDto> getAll() {
        return commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
	}
	
	@POST
	public Response createCommande(CommandeCreateDto commandeCreateDto) {
		Commande existing = commandes.findByName(commandeCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
		if(commandeCreateDto.getName() == null || commandeCreateDto.getName().equals("")) throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		try {
			Commande commande = Commande.fromCommandeCreateDto(commandeCreateDto);
			commandes.insert(commande);
			CommandeDto ingredientDto = Commande.toDto(commande);

			URI uri = uriInfo.getAbsolutePathBuilder().path(commande.getId().toString()).build();

			return Response.created(uri).entity(ingredientDto).build();
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteCommande(@PathParam("id") UUID id) {
		if ( commandes.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		commandes.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}
	
	@GET
	@Path("{id}/name")
	public String getCommandeName(@PathParam("id") UUID id) {
		Commande commande = commandes.findById(id);

		if (commande == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return commande.getName();
	}
	
	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createIngredient(@FormParam("name") String name) {
		Commande existing = commandes.findByName(name);
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Commande commande = new Commande();
			commande.setName(name);
			commande.setPizzas(new ArrayList<Pizza>());
			commandes.insert(commande);

			CommandeDto commandeDto = Commande.toDto(commande);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + commande.getId()).build();

			return Response.created(uri).entity(commandeDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

}
