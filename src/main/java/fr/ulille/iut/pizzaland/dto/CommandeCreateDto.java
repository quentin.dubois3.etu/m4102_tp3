package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Pizza;

public class CommandeCreateDto {
	private String name;
	private List<Pizza> pizzas;
		
	public CommandeCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}
	
	public void setPizzas(List<Pizza> pizzas) {
		this.pizzas = pizzas;
	}
	
	public List<Pizza> getPizzas(){
		return this.pizzas;
	}
}
