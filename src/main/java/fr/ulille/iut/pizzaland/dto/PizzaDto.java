package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {

	private UUID id;
    private String name;
    private double price;
    private List<Ingredient> ingredients;

    public PizzaDto() {
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
    	return name;
    }
    
    public void setPrice(double price) {
    	this.price = price;
    }
    
    public double getPrice() {
    	return this.price;
    }

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public List<Ingredient> getIngredients(){
		return this.ingredients;
	}

}