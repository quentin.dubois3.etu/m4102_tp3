package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private UUID id = UUID.randomUUID();
    private String name;
    private double price;
    private List<Ingredient> ingredients = new ArrayList<>();

    public Pizza() {}
    
    public Pizza(String name) {
        this(UUID.randomUUID(), name);
    }

    public Pizza(UUID id, String name) {
        this(id, name, 0);
    }
    
    public Pizza(UUID id, String name, double price) {
    	this.id = id;
        this.name = name;
    	this.price = price;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
    	return this.price;
    }
    
    public void setIngredients(List<Ingredient> ingredients) {
    	this.ingredients = ingredients;
    }
    

    public List<Ingredient> getIngredients() {
    	return this.ingredients;
    }
    
    public void setPrice(double price) {
    	this.price = price;
    }
    
    

    public static PizzaDto toDto(Pizza p) {
        PizzaDto dto = new PizzaDto();
        dto.setId(p.getId());
        dto.setName(p.getName());
        dto.setPrice(p.getPrice());
        dto.setIngredients(p.getIngredients());

        return dto;
    }
    
    public static Pizza fromDto(PizzaDto dto) {
        Pizza p = new Pizza();
        p.setId(dto.getId());
        p.setName(dto.getName());
        p.setPrice(dto.getPrice());
        p.setIngredients(dto.getIngredients());
        return p;
    }

    @Override
    public String toString() {
        return "Pizza [id=" + id + ", name=" + name + ", price = " + price + "Ingredients : " + ingredients;
    }
    
    public static PizzaCreateDto toCreateDto(Pizza pizza) {
    	PizzaCreateDto dto = new PizzaCreateDto();
    	dto.setName(pizza.getName());
    	dto.setPrice(pizza.getPrice());

    	return dto;
    }

    public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
    	Pizza pizza = new Pizza();
    	pizza.setName(dto.getName());
    	pizza.setPrice(dto.getPrice());
    	pizza.setIngredients(dto.getIngredients());
    	return pizza;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(price);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(price) != Double.doubleToLongBits(other.price))
			return false;
		return true;
	}

	
}
