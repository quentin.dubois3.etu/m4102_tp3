package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.resources.PizzaRessource;

public interface CommandeDao {
	
	public static final Logger LOGGER = Logger.getLogger(PizzaRessource.class.getName());
	public static final PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
	
	@Transaction
    default void createTable() {
		pizzaDao.createTable();
		this.createCommandeTable();
		this.createAssociationTable();
    }
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS commandes (id VARCHAR(128) PRIMARY KEY, "
    		+ "name VARCHAR UNIQUE NOT NULL);")
    void createCommandeTable();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS associationCommandes (idCommande VARCHAR(128), idPizza VARCHAR, "
			+ "CONSTRAINT pk_assosCommande PRIMARY KEY (idCommande, idPizza),"
			+ "FOREIGN KEY(idPizza) REFERENCES pizzas(id),"
			+ "FOREIGN KEY(idCommande) REFERENCES commandes(id))")
		    void createAssociationTable();
	
	@Transaction
	default void dropTable() {
    	this.dropTableAssoc();
    	this.dropTableCommande();
    }
	
	@SqlUpdate("DROP TABLE IF EXISTS association")
    void dropTableAssoc();
    @SqlUpdate("DROP TABLE IF EXISTS commandes")
    void dropTableCommande();
    
    @Transaction
    default void insert(Commande c) {
    	this.insertCommande(c);
    	if(c.getPizzas() != null) for(Pizza p : c.getPizzas()) this.addPizzaToCommande(c, p);
    }
    
    @SqlUpdate("INSERT INTO associationCommandes(idCommande, idPizza) values(:commande.id, :pizza.id);")
    void addPizzaToCommande(@BindBean("commande") Commande commande, @BindBean("pizza") Pizza pizza);
    
    @SqlUpdate("INSERT INTO commandes (id, name) VALUES (:id, :name); ")
    void insertCommande(@BindBean Commande commande);

    
    @SqlQuery("SELECT * FROM commandes WHERE id = :id")
    @RegisterBeanMapper(Commande.class)
    Commande loadById(@Bind("id") UUID id);
    
    @SqlQuery("SELECT idPizza FROM associationCommandes WHERE idCommande = :id")
    List<String> loadPizzaIDFromCommandeID(@Bind("id") UUID id);
    
    @Transaction
    default List<Pizza> loadPizzaFromCommandeID(UUID id){
    	List<Pizza> res = new ArrayList<Pizza>();
    	for(String s : loadPizzaIDFromCommandeID(id)) {
    		res.add(pizzaDao.loadById(UUID.fromString(s)));
    	}
    	return res;
    }
    
    @Transaction
    default Commande findById(UUID id) {
    	Commande c = loadById(id);
    	if(c!=null) {
    		List<Pizza> tmp = loadPizzaFromCommandeID(id);
    		c.setPizzas((tmp==null)?new ArrayList<>():tmp);
    	}
    	return c;
    }
    
    @SqlQuery("SELECT * FROM commandes")
    @RegisterBeanMapper(Commande.class)
    List<Commande> loadAll();
    
    @Transaction
    default List<Commande> getAll(){
    	List<Commande> commandes = loadAll();
    	for(Commande c : commandes) {
    		c.setPizzas(loadPizzaFromCommandeID(c.getId()));
    	}
    	return commandes;
    }
    
    @SqlQuery("SELECT * FROM commandes WHERE name = :name")
    @RegisterBeanMapper(Commande.class)
    Commande loadByName(@Bind("name") String name);
    
    @Transaction
    default Commande findByName(String name) {
    	Commande c = loadByName(name);
    	if(c==null) return null;
    	c.setPizzas(loadPizzaFromCommandeID(c.getId()));
    	return c;
    }
    
    @Transaction
    default void remove(UUID id) {
    	removeFromAssociation(id);
    	removeFromCommande(id);
    }
    
    @SqlUpdate("DELETE FROM associationCommandes WHERE idCommande = :id")
    void removeFromAssociation(@Bind("id") UUID id);
    
    @SqlUpdate("DELETE FROM commandes WHERE id = :id")
    void removeFromCommande(@Bind("id") UUID id);
}
