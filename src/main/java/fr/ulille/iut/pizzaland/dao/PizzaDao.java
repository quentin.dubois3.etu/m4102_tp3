package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.resources.PizzaRessource;

public interface PizzaDao {
	
	public static final Logger LOGGER = Logger.getLogger(PizzaRessource.class.getName());
	
	@Transaction
    default void createTable() {
		createPizzaTable();
		createIngredientTable();
		createAssociationTable();
    }
    
    @SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, "
    		+ "name VARCHAR UNIQUE NOT NULL, price DOUBLE );")
    void createPizzaTable();
    
    @SqlUpdate("CREATE TABLE IF NOT EXISTS association (idpizza VARCHAR(128), nomingredient VARCHAR, "
	+ "CONSTRAINT pk_assos PRIMARY KEY (idpizza, nomingredient),"
	+ "FOREIGN KEY(idpizza) REFERENCES pizzas(id),"
	+ "FOREIGN KEY(nomingredient) REFERENCES ingredients(name))")
    void createAssociationTable();
    
    @SqlUpdate("CREATE TABLE IF NOT EXISTS ingredients (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createIngredientTable();

    @Transaction
    default void dropTable() {
    	this.dropTableAssoc();
    	this.dropTablePizza();
    }
    
    @SqlUpdate("DROP TABLE IF EXISTS association")
    void dropTableAssoc();
    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropTablePizza();

    @SqlUpdate("INSERT INTO pizzas (id, name, price) VALUES (:id, :name, :price); ")
    void insertPizza(@BindBean Pizza pizza);

    @SqlUpdate("INSERT INTO association(idpizza, nomingredient) values(:pizza.id, :ingredient.name);")
    void addIngredientToPizza(@BindBean("pizza") Pizza pizza, @BindBean("ingredient") Ingredient ingredient);
    
    @Transaction
    default void insert(Pizza p) {
    	this.insertPizza(p);
    	if(p.getIngredients() != null) for(Ingredient i : p.getIngredients()) this.addIngredientToPizza(p, i);
    }
    
    @Transaction
    default void remove(UUID id) {
    	removeFromAssociation(id);
    	removeFromPizza(id);
    }
    
    @SqlUpdate("DELETE FROM association WHERE idpizza = :id")
    void removeFromAssociation(@Bind("id") UUID id);
    
    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void removeFromPizza(@Bind("id") UUID id);

//    @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
//    @RegisterBeanMapper(Pizza.class)
//    Pizza findByName(@Bind("name") String name);

    @SqlQuery("SELECT * FROM pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> loadAll();
    
    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza loadById(@Bind("id") UUID id);
    
    @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza loadByName(@Bind("name") String name);
    
    @SqlQuery("SELECT i.id, i.name FROM association as a INNER JOIN ingredients as i ON(i.id = a.nomingredient) where a.idpizza = :id")
	@RegisterBeanMapper(Ingredient.class)
    List<Ingredient> loadIngredientsFromPizzaID(@Bind("id") UUID id);

    @Transaction
    default Pizza findById(UUID id) {
    	Pizza p = loadById(id);
    	if(p!=null) {
    		List<Ingredient> tmp = loadIngredientsFromPizzaID(id);
    		p.setIngredients((tmp==null)?new ArrayList<>():tmp);
    	}
    	return p;
    }

    @Transaction
    default Pizza findByName(String name) {
    	Pizza p = loadByName(name);
    	if(p==null) return null;
    	p.setIngredients(loadIngredientsFromPizzaID(p.getId()));
    	return p;
    }

    @Transaction
    default List<Pizza> getAll(){
    	List<Pizza> pizzas = loadAll();
    	for(Pizza p : pizzas) {
    		p.setIngredients(loadIngredientsFromPizzaID(p.getId()));
    	}
    	return pizzas;
    }
    
    default void insertMany(List<Pizza> pizzas) {
    	for(Pizza pizza : pizzas) this.insert(pizza);
    }
    
    
}
